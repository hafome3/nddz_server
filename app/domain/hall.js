/**
 * Created by Mic on 16/2/22.
 */
var _ = require('underscore');
var logger = require('pomelo-logger').getLogger(__filename);
var Room = require('./room');
var consts = require('../consts/consts');
var pomelo = require('pomelo');

var Hall = function () {
    this.playerList = [];
    this.roomList = [];
    this.initRooms();

    var s = function(){
        console.error('new Hall----',pomelo.app.get('serverId'));
    };
    s();
};

var handler = Hall.prototype;

//大厅的操作
handler.enterHall = function (player) {
    console.log('enter hall ',this.playerList);
    var exit = _.find(this.playerList, function (p) {
        return player.uuid == p.uuid;
    });

    if (!!exit) {
        logger.fatal('hall has this player ' + player);
    } else {
        this.playerList.push(player);
    }
};

handler.quitHall = function (player) {
    //找到当前玩家的room
    //退出游戏部分 TODO 现在欠考虑的就是如果在游戏中 hall room中玩家的记录到底该不该清除.
    var room = _.find(this.roomList, function (r) {
        return _.find(r.getPlayerList(), function (p) {
            if (p) {
                return player.uuid == p.uuid;
            }
        });
    });
    if(!!room){
        room.exitRoom(player);
    }

    var playerList = this.playerList.slice(0);
    playerList = _.filter(playerList, function (p) {
        return player.uuid != p.uuid;
    });
    this.playerList = playerList;
    this.getPlayerCounts();
};

//初始化场次
handler.initRooms = function () {
    this.room1 = new Room(consts.Room.Normal.Room1);
    this.room2 = new Room(consts.Room.Normal.Room2);
    this.room3 = new Room(consts.Room.Normal.Room3);
    this.roomList.push(this.room1, this.room2, this.room3);
};

handler.getRoomById = function (roomId) {
    switch (roomId) {
        case consts.Room.Normal.Room1.id:
            return this.room1;
            break;
        case consts.Room.Normal.Room2.id:
            return this.room2;
            break;
        case consts.Room.Normal.Room3.id:
            return this.room3;
            break;
        default:
            return null;
            break;
    }
};
handler.getRoomByName = function (roomName) {
    switch (roomName) {
        case consts.Room.Normal.Room1.name:
            return this.room1;
            break;
        case consts.Room.Normal.Room2.name:
            return this.room2;
            break;
        case consts.Room.Normal.Room3.name:
            return this.room3;
            break;
        default:
            return null;
            break;
    }
};
//当前在线人数
handler.getPlayerCounts = function () {
    console.log(this.playerList);
    logger.trace('当前在线人数 [ ' + this.playerList.length + ' ]');
    return this.playerList.length;
};

//根据你现在钱限制玩家能进的场次 return roomId
handler.checkMoney = function (money) {
    var msg = null;
    if (money < 1500) {
        msg = null;
    } else {
        msg = consts.Room.Normal.Room1.id;
    }
    return msg;
};
handler.checkEnterRoom = function (roomName, money, next) {
    var msg = null;

    if (money < 10000) {
        msg = 1;
    }

    switch (roomName) {
        case 'room1':
            if (money < 100000) {
                next(null, {
                    code: consts.Code.Ok,
                    serverId: consts.Room.Normal.Room1.serverId
                });
            } else {
                msg = 2;
            }
            break;
        case 'room2':
            if (money < 100000) {
                msg = 1;
            } else if (money > 500000) {
                msg = 2;
            } else {
                next(null, {
                    code: consts.Code.Ok,
                    serverId: consts.Room.Normal.Room2.serverId
                });

            }
            break;
        case 'room3':
            if (money < 500000) {
                msg = 1;
            } else {
                next(null, {
                    code: consts.Code.Ok,
                    serverId: consts.Room.Normal.Room3.serverId
                });
            }
            break;
        default:
            msg = 0;
            break;
    }
    if (msg != null) {
        next(null, {
            code: 500,
            msg: msg
        });
    }
};

handler.setPlayerList = function(){

};

handler.getPlayerList = function(){
    return this.playerList;
};

module.exports = Hall;

//Hall.instance = null;
//Hall.getInstance = function () {
//    if (!Hall.instance) {
//        console.error('new hall',pomelo.app.get('serverId'));
//        Hall.instance = new Hall();
//    }
//    return this.instance;
//};
//
//module.exports = Hall.getInstance();

