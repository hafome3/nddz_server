/**
 * Created by Mic on 16/2/22.
 */

var node_uuid = require('node-uuid');

var Character = function (seatIndex, info) {
    console.warn(info);
    this.userId = info.userId ? info.userId : null;
    this.scode = info.scode ? info.scode : null;
    this.uuid = info.uuid ? info.uuid : node_uuid.v1();
    this.roomName = info.roomName;
    this.deskName = info.deskName;
    this.userName = info.userName;
    this.money = info.money;
    this.sid = info.sid ? info.sid : null;

    this.headUrl = info.headUrl ? info.headUrl : null;

    this.seatIndex = seatIndex;
    this.cards = [];
    this.dizhu = false;
    this.beishu = 1;
    this.playRound = 0;
    this.readyStatus = 0;       //准备或者明牌开始
    this.pointStatus = false;   //叫地主的状态
    this.doubleStatus = 0;      //加倍的状态

    this._robot = null;         //托管
    this.deskClient = null;
    this.timing = null;
};
Character.prototype.baseInfo = function () {
    return {
        uuid: this.uuid,
        userId: this.userId,
        scode: this.scode,
        roomName: this.roomName,
        deskName: this.deskName,
        userName: this.userName,
        money: this.money,
        headUrl: this.headUrl,
        cards: this.cards,
        playerRound: this.playRound,
        seatIndex: this.seatIndex,
        readyStatus: this.readyStatus,
        beishu: this.beishu,
        doubleStatus: this.doubleStatus
    };
};

Character.prototype.reset = function () {
    //this.deskClient = null;
    this.cards = [];
    this.dizhu = false;
    this.beishu = 1;
    this.playRound = 0;
    this.readyStatus = 0;    //准备或者明牌开始
    this.pointStatus = false;  //叫地主的状态
    this.doubleStatus = 0;      //加倍的状态
    this._robot = null;
    if (!!this.timing) {
        clearTimeout(this.timing);
    }
};
module.exports = Character;