/**
 * Created by Mic on 16/2/22.
 */

var _ = require('underscore');
var logger = require('pomelo-logger').getLogger(__filename);
var Desk = require('./desk/desk');
var consts = require('../consts/consts');
var channelUtil = require('../util/channelUtil');
var pomelo = require('pomelo');

var Room = function (opts) {
    this.name = opts.name;
    this.roomId = opts.id;
    this.baseBet = opts.baseBet;
    this.money = opts.money;
    this.serverId = opts.serverId;
    this.ticket = opts.ticket;
    this.robot = opts.robot;

    this.deskList = [];
    this.playerList = [];
    this.deskId = 0;
    this.channel = null;

    this.baseInfo = function () {
        return {
            id: this.roomId,
            name: this.name,
            baseBet: this.baseBet,
            money: this.money,
            ticket: this.ticket,
            beishu: 1
        };
    };
};

var handler = Room.prototype;

handler.enterRoom = function (player) {
    logger.debug('进入 ' + this.name + ' 游戏场');
    var p = _.find(this.playerList, function (p) {
        return p.uuid == player.uuid;
    });

    if (!!p) {
        logger.warn(player.uuid + '已经在游戏中.请回到之前的桌子');
    } else {
        this.playerList.push(player);
    }
};
handler.backDesk = function (player, deskName) {
    var desk = _.find(this.deskList, function (d) {
        return d.name == deskName;
    });
    if (!!desk) {
        return desk.playerConnected(player);
    }
};
handler.reConnect = function(player,deskName){
    var desk = _.find(this.deskList, function (d) {
        return d.name == deskName;
    });
    if (!!desk) {
        return desk.reConnect(player);
    }
};
handler.exitRoom = function (player) {
    var desk = this.findDeskByUuid(player.uuid);
    console.warn('想退出房间? ');
    //退出桌子  1,游戏中 2,桌子未满 3,不在桌子内
    if (!!desk) {
        logger.trace('正在游戏中');
        if (desk.getStatus() == consts.Desk.Status.Gaming) {
            desk.playerDisconnect(player);
            return false;
        } else if (desk.getStatus() == consts.Desk.Status.NotFull
            || desk.getStatus() == consts.Desk.Status.Full) {
            desk.leave(player);
            return true;
        }
    } else {
        logger.fatal('不在桌子内,安心的离开');
        this.quitRoom(player);
        return true;
    }
};
handler.quitRoom = function (player) {
    this.playerList = _.filter(this.playerList, function (p) {
        return p.uuid != player.uuid;
    });
};
handler.getChannel = function () {
    if (!this.channel) {
        var channelName = channelUtil.getRoomChannelName(this.roomId);
        this.channel = pomelo.app.get('channelService').getChannel(channelName, true);
    }
    return this.channel;
};
//desk操作
handler.createDesk = function () {
    var id = this.deskId++;
    var desk = new Desk({
        id: id,
        roomName: this.name,
        name: this.name + '_' + id,
        baseBet: this.baseBet,
        money: this.money,
        serverId: this.serverId,
        ticket: this.ticket,
        robot: this.robot
    });
    this.deskList.push(desk);
    return desk;
};
handler.deleteDesk = function (deskName) {
    if (!!deskName) {
        logger.fatal('删除桌子 ' + deskName);
        var channelService = pomelo.app.get('channelService');
        channelService.destroyChannel(deskName);
        //删除桌子之前删除掉掉线的用户
        var deskList = this.deskList.slice(0);
        deskList = _.filter(deskList, function (d) {
            return d.name != deskName;
        });
        this.deskList = deskList;
    }
};
//寻找一个未满的桌子 |如果没有则创建一个
handler.findEmptyDesk = function () {
    var desk = _.find(this.deskList, function (d) {
        if (d) {
            return d.status == consts.Desk.Status.NotFull;
        }
        return null;
    });

    return !!desk ? desk : this.createDesk();
};

handler.findDeskById = function (deskId) {
    return _.find(this.deskList, function (d) {
        return d.id == deskId;
    });
};

handler.findDeskByUuid = function (uuid) {
    return _.find(this.deskList, function (d) {
        return _.find(d.playerList, function (p) {
            if (p) {
                return p.uuid == uuid;
            }else{
                return null;
            }
        })
    });
};
handler.findDeskByName = function (name) {
    return _.find(this.deskList, function (d) {
        return d.name == name;
    });
};

handler.getRoomPlayerCounts = function () {
    logger.fatal(this.name + '当前房间个数 ' + this.deskList.length);
    logger.fatal(this.name + '当前在线玩家人数 ' + this.playerList.length);
};
handler.getPlayerList = function () {
    //console.warn('test');
    return this.playerList;
};
handler.test = function () {
    console.error('test');
};
module.exports = Room;