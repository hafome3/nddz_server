
var ChannelUtil = module.exports;

var GLOBAL_CHANNEL_NAME = 'ddz_global';
var ROOM_CHANNEL_NAME = 'ddz_room_';
var DESK_CHANNEL_NAME = 'ddz_desk_';
var FRIEND_CHANNEL_NAME = 'wetchat'; //私信

ChannelUtil.getGlobalChannelName = function() {
    return GLOBAL_CHANNEL_NAME;
};

ChannelUtil.getRoomChannelName = function(roomId) {
    return ROOM_CHANNEL_NAME + roomId;
};

ChannelUtil.getDeskChannelName = function(deskId) {
    return DESK_CHANNEL_NAME + deskId;
};